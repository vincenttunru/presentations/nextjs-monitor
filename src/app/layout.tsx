import type { Metadata } from "next";
import { Sarala } from "next/font/google";
import "./globals.css";

const bodyFont = Sarala({ weight: "400", subsets: ["latin"] });

export const metadata: Metadata = {
  title: "React on the server — what's it like in practice?",
  description: "A presentation by Vincent Tunru",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en" className="scroll-smooth">
      <body className={bodyFont.className}>{children}</body>
    </html>
  );
}
