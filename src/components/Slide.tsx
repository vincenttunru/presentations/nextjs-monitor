import { ReactNode, useEffect, useRef } from "react";

export const Slide = (props: {
  children: ReactNode;
  isCurrent: boolean;
  hidden: boolean;
}) => {
  const ref = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (props.isCurrent) {
      ref.current?.scrollIntoView();
    }
  }, [props.isCurrent]);

  return (
    <div
      ref={ref}
      aria-hidden={props.hidden}
      className="flex-shrink-0 snap-center p-20 md:p-32 xl:p-40 pb-60 flex flex-col items-center justify-center gap-10 text-xl md:text-2xl xl:text-4xl w-screen h-full bg-amber-200"
    >
      {props.children}
    </div>
  );
};
