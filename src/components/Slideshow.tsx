"use client";

import {
  ReactNode,
  useState,
  KeyboardEventHandler,
  cloneElement,
  isValidElement,
  useEffect,
  useRef,
  MouseEventHandler,
} from "react";
import { Slide } from "./Slide";

const idleTimeoutMs = 2 * 1000;

export const Slideshow = (props: {
  slides: ReactNode[];
  attribution: ReactNode;
}) => {
  const [currentSlide, setCurrentSlide] = useState(0);
  const [isBlank, setIsBlank] = useState(false);
  const slideshowRef = useRef<HTMLDivElement>(null);
  const [isIdle, setIsIdle] = useState(false);
  const idleTimerRef = useRef<ReturnType<typeof setTimeout>>();

  const resetIdleTimer = () => {
    setIsIdle(false);
    clearTimeout(idleTimerRef.current);
    idleTimerRef.current = setTimeout(() => {
      setIsIdle(true);
    }, idleTimeoutMs);
  };

  const goToNextSlide = () => {
    setCurrentSlide((prev) => Math.min(prev + 1, props.slides.length - 1));
  };
  const goToPreviousSlide = () => {
    setCurrentSlide((prev) => Math.max(prev - 1, 0));
  };

  const onKeyPress: KeyboardEventHandler = (event) => {
    if (event.key === "ArrowRight" || event.key === "PageDown") {
      event.preventDefault();
      goToNextSlide();
    }

    if (event.key === "ArrowLeft" || event.key === "PageUp") {
      event.preventDefault();
      goToPreviousSlide();
    }

    if (event.key === "b") {
      event.preventDefault();
      setIsBlank((wasBlank) => !wasBlank);
    } else if (isBlank) {
      setIsBlank(false);
    }

    if (event.key === "F5" && event.shiftKey) {
      // Prevent the wrong key on my clicker opening the network tools
      event.preventDefault();
    }
  };

  const onMouseMove: MouseEventHandler = (_event) => {
    resetIdleTimer();
  };

  useEffect(() => {
    document.addEventListener("keydown", onKeyPress as any);

    return () => {
      document.removeEventListener("keydown", onKeyPress as any);
    };
  });

  return (
    <div
      tabIndex={0}
      className={`h-full flex snap-x snap-mandatory relative selection:bg-amber-700 ${
        isIdle ? "cursor-none" : ""
      }`}
      ref={slideshowRef}
      onMouseMove={onMouseMove}
    >
      {props.slides.map((slide, i) => {
        return (
          <Slide key={i} isCurrent={i === currentSlide} hidden={isBlank}>
            {slide}
          </Slide>
        );
      })}
      <div
        className={`fixed w-screen h-screen bg-amber-200 transition-all duration-500 ${
          isBlank ? "" : "opacity-0 touch-none"
        }`}
      />
      <aside>
        <button
          onClick={() => goToPreviousSlide()}
          aria-label="Previous slide"
          hidden={currentSlide === 0}
          className={`fixed left-0 h-screen px-5 lg:px-8 xl:px-20 text-6xl xl:text-9xl font-bold transition-opacity duration-500 opacity-0 ${
            isIdle ? "cursor-none" : "hover:opacity-100"
          }`}
        >
          <span className="inline-block rotate-180">➜</span>
        </button>
        <button
          onClick={() => goToNextSlide()}
          aria-label="Next slide"
          hidden={currentSlide === props.slides.length - 1}
          className={`fixed right-0 h-screen px-5 lg:px-8 xl:px-20 text-6xl xl:text-9xl font-bold transition-opacity duration-500 opacity-0 ${
            isIdle ? "cursor-none" : "hover:opacity-100"
          }`}
        >
          <span className="inline-block">➜</span>
        </button>
      </aside>
      <footer className="fixed w-full bottom-0 flex gap-5 items-end p-10">
        {isValidElement(props.attribution)
          ? cloneElement(props.attribution, {
              className: `not-italic transition-all font-bold ${
                currentSlide === 0 || currentSlide === props.slides.length - 1
                  ? "text-xl md:text-3xl xl:text-5xl"
                  : "text-lg md:text-2xl"
              }`,
            } as any)
          : null}
        <div className="flex-grow whitespace-nowrap text-end text-lg font-bold">
          {currentSlide + 1} / {props.slides.length}
        </div>
      </footer>
    </div>
  );
};
