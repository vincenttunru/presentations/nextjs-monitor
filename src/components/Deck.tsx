import { ReactNode, Children, isValidElement } from "react";
import { Slideshow } from "./Slideshow";

export const Deck = ({ children }: { children: ReactNode }) => {
  const deck = getDeck(children);
  return <Slideshow slides={deck.slides} attribution={deck.attribution} />;
};

const getDeck = (children: ReactNode) => {
  const slides: ReactNode[][] = [[]];
  let attribution: ReactNode = null;
  let slideIndex = 0;
  Children.forEach(children, (child) => {
    if (isValidElement(child) && child.type === "hr") {
      slideIndex++;
      slides[slideIndex] = [];
      return;
    }
    if (isValidElement(child) && child.type === "address") {
      attribution = child;
      return;
    }
    slides[slideIndex].push(child);
    return;
  });

  return { slides, attribution };
};
