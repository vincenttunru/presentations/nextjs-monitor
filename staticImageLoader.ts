export default function staticImageLoader({
  src,
  width,
  quality,
}: {
  src: string;
  width: number;
  quality?: number;
}) {
  // We do static HTML export, so we can't dynamically optimise images.
  // Just load them from `/public`.
  return src;
}
