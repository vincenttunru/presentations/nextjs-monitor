import type { MDXComponents } from "mdx/types";
import { Lilita_One } from "next/font/google";
import {
  DetailedHTMLProps,
  HTMLAttributes,
  ImgHTMLAttributes,
  Suspense,
  lazy,
} from "react";
import Image from "next/image";

const titleFont = Lilita_One({ weight: "400", subsets: ["latin"] });

export function useMDXComponents(components: MDXComponents): MDXComponents {
  return {
    h1: (props) => (
      <h1
        {...props}
        className={`${props.className ?? ""} ${
          titleFont.className
        } text-5xl md:text-7xl xl:text-9xl text-center`}
      />
    ),
    h2: (props) => (
      <h2
        {...props}
        className={`${
          props.className ?? ""
        } font-bold text-3xl md:text-4xl xl:text-6xl`}
      />
    ),
    ul: (props) => (
      <ul
        {...props}
        className={`${props.className ?? ""} list-disc flex flex-col gap-5`}
      />
    ),
    ol: (props) => (
      <ol
        {...props}
        className={`${props.className ?? ""} list-decimal flex flex-col gap-5`}
      />
    ),
    table: (props) => (
      <table
        {...props}
        className={`${
          props.className ?? ""
        } border-separate border-spacing-x-10 border-spacing-y-5`}
      />
    ),
    blockquote: (props) => (
      <blockquote {...props} className={`${props.className ?? ""} italic`} />
    ),
    img: (props) => <LazyLoadImage {...props} />,
    a: (props) => (
      <a {...props} className={`${props.className ?? ""} text-amber-800`} />
    ),
    ...components,
  };
}

const LazyLoadImage = (
  props: DetailedHTMLProps<
    ImgHTMLAttributes<HTMLImageElement>,
    HTMLImageElement
  >,
) => {
  const getImage = async () => {
    const imgData = await import(`./public/${props.src}`);
    const ImageComponent = () => (
      <Image
        src={imgData.default}
        alt={props.alt ?? ""}
        className={`${props.className ?? ""} max-w-screen max-h-screen`}
      />
    );
    return {
      default: ImageComponent,
    };
  };
  const LazyImage = lazy(getImage);

  return (
    <Suspense fallback={null}>
      <LazyImage />
    </Suspense>
  );
};
