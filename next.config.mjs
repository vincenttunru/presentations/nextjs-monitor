import remarkGfm from "remark-gfm";
import createMDX from "@next/mdx";

const withMDX = createMDX({
  options: {
    remarkPlugins: [remarkGfm],
    rehypePlugins: [],
  },
});

const basePath = process.env.CI_PAGES_URL?.substring(
  process.env.CI_PAGES_URL.indexOf(process.env.CI_PAGES_DOMAIN) +
    process.env.CI_PAGES_DOMAIN.length,
);

/** @type {import('next').NextConfig} */
const nextConfig = {
  pageExtensions: ["js", "jsx", "mdx", "ts", "tsx"],
  output: "export",
  images: {
    loader: "custom",
    loaderFile: "./staticImageLoader.ts",
    unoptimized: true,
  },
  basePath: basePath?.endsWith("/")
    ? basePath.substring(0, basePath.length - 1)
    : basePath,
};

export default withMDX(nextConfig);
